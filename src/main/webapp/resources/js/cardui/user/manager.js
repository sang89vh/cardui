function initDatatable(){
	console.log("call user-datatable");
	var tableId = "user-datatable";
	var table = $('#'+tableId).DataTable( {
		 "language": {
	            "url": "../resources/lib/DataTables/plugins/Vietnamese.json"
	        },
	        
	        "processing": true,
	        "autoWidth": false,
	        "paging": false,
        "ajax": {
			url: "../user/all?"+csrfname+"="+csrfvalue,
			data: function ( d ) {
				console.log(d);
    			d = JSON.stringify(d);
    			return d;
            },
        	type : "POST",
        	contentType : 'application/json',
    		mimeType: 'application/json'
        },
         "columns": [
		            { data: "user_id",searchable: true,orderable:true,type:"string"},
		            { data: "user_name",searchable: true,orderable:true,type:"string"},
		            { data: "role",searchable: true,orderable:true,type:"string"},
		            { data: "enabled",searchable: true,orderable:true,type:"string",render: function(data, type, row, meta) {
							var status = null;
							if (row.enabled === 1) {
								status =  '<span class="label label-primary">Enable</span>';
							} else if (row.enabled == 0) {
								status =  '<span class="label label-danger">Disable</span>';
							}
							return '<span class="wrapword">'+status+'</span>';
						}
					},
		            { data: "user_id",searchable: false,orderable:false,type:"number",render: function ( data, type, row, meta ) {
	            		return '<button class="btn btn-info edit-btn btn-xs" id="editBtn" style="margin-right:5px;">Edit</button>'+
	            		'<button class="btn btn-danger edit-btn btn-xs" id="deleteBtn">Disable</button>';
	            	}
					}
				
        ]
    } );
}

$(document).ready(function(){
	initDatatable();
})
