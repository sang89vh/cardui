<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/layout/taglib.jsp"%>
<link href="${ctx}/resources/css/cardui/user/manager.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/resources/js/cardui/user/manager.js" type="text/javascript"></script>
<!-- datatable place holder-->
<div id="user-manager">
	<table id="user-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>#</th>
				<th>User name</th>
				<th>Role</th>
				<th>Enabled</th>
				<th>Action</th>
				<th></th>
			</tr>
		</thead>
	</table>
</div>