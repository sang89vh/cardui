<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/layout/taglib.jsp"%>
<link  href="${ctx}/resources/css/user/user01.css" rel="stylesheet" type="text/css" />
	<div class="div-login-container">
		<form name='loginForm' id="form-signup" class="form-signup"
			action="<c:url value='/login' />" method="post">
			<div >
				<c:if test="${not empty error}">
					<div class="error" style="text-align: center;">${error}</div>
				</c:if>
				<c:if test="${not empty msg}">
					<div class="msg" style="text-align: center;">${msg}</div>
				</c:if>
			</div>
			<h5 class="form-signup-heading text-center">LOGIN TO CARD UI</h5>
			<div class="col-md-12" style="margin-bottom: 10px;">
			<label for="inputEmail" class="sr-only">Your email</label> 
					<input type='text' 
						   name='username' 
						   id='username' 
						   class='form-control'
						   placeholder="Enter your email" 
						   required 
						   autofocus />
		      </div>
		      <div class="col-md-12" style="margin-bottom: 10px;">
			<label for="inputpassword" class="sr-only">Your password</label> 
			<input type='password' 
				   name='password'
				   id="inputpassword"
				   placeholder="Enter your password" 
				   required 
				   class="form-control"
				   />
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			</div>
			<div class="col-md-12" style="margin-bottom: 10px;">
			     <button id="btn-signup-submit" 	data-loading-text="Loading..." class="btn btn-lg btn-primary btn-block" type="submit" >Login</button>
            </div>
		</form>
	</div>
