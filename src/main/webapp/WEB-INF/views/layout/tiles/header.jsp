<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/layout/taglib.jsp"%>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="../../favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<title>CardUI</title>
<script type="text/javascript"> 
	var ctx = "<%=request.getContextPath()%>";
</script>
<link  href="${ctx}/resources/css/CardUI.css" rel="stylesheet" type="text/css" />
<link  href="${ctx}/resources/css/sweetalert.css" rel="stylesheet" type="text/css" />
<link  href="${ctx}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link  href="${ctx}/resources/css/pace.css" rel="stylesheet" type="text/css" />
<link  href="${ctx}/resources/css/sweetalert.css" rel="stylesheet" type="text/css" />
<link  href="${ctx}/resources/css/flat-ui.min.css" rel="stylesheet" type="text/css" />
<link   href="${ctx}/resources/css/jsgrid.min.css" rel="stylesheet" type="text/css"/>
<link   href="${ctx}/resources/css/jsgrid-theme.min.css" rel="stylesheet" type="text/css"/>
<link   href="${ctx}/resources/css/amchart.css" rel="stylesheet" type="text/css"/>
<link   href="${ctx}/resources/css/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
<link   href="${ctx}/resources/css/fullcalendar.print.css" rel='stylesheet' media='print' type="text/css"/>
<!-- Add fancyBox -->
<link   href="${ctx}/resources/css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
<!-- Optionally add helpers - button, thumbnail and/or media -->
<link   href="${ctx}/resources/css/jquery.fancybox-buttons.css" rel="stylesheet" type="text/css" />
<link   href="${ctx}/resources/css/jquery.fancybox-thumbs.css" rel="stylesheet" type="text/css" />

<!-- datatable -->
<link href="${ctx}/resources/lib/DataTables/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/resources/lib/DataTables/css/select.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/resources/lib/DataTables/css/dataTables.checkboxes.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/resources/lib/DataTables/css/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/resources/lib/DataTables/css/fixedHeader.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/resources/lib/DataTables/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/resources/lib/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

<link  href="${ctx}/resources/css/CardUI.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="${ctx}/resources/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/tether.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/pace.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/flat-ui.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/sweetalert.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/jsgrid.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/moment.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/fullcalendar.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/amcharts.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/serial.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/amstock.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/bootstrap-notify.min.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="${ctx}/resources/js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="${ctx}/resources/js/jquery.fancybox-thumbs.js"></script>

<!-- datatable -->
<script type="text/javascript" src="${ctx}/resources/lib/DataTables/js/jszip.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/lib/DataTables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/lib/DataTables/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/lib/DataTables/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/lib/DataTables/js/dataTables.rowGroup.js"></script>
<script type="text/javascript" src="${ctx}/resources/lib/DataTables/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/lib/DataTables/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/lib/DataTables/js/moment.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/lib/DataTables/js/datetime-moment.js"></script>
<script type="text/javascript" src="${ctx}/resources/lib/DataTables/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="${ctx}/resources/lib/DataTables/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="${ctx}/resources/js/CardUI.js"></script>

<script type="text/javascript">
var csrfname="${_csrf.parameterName}";
var csrfvalue="${_csrf.token}";
</script>

