package com.card.api.CardUI.controller;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.card.api.CardUI.dto.BaseDatatableDto;
import com.card.api.CardUI.dto.DatatableDto;
import com.card.api.CardUI.model.UserModel;
import com.card.api.CardUI.service.IUserService;

@Controller
@SessionAttributes("userController")
@RequestMapping("/user")
public class UserController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

	@Autowired
	private IUserService userService;
	@RequestMapping(value = "/manager", method = RequestMethod.GET)
	public ModelAndView manager(ModelAndView mav) {

		mav.setViewName("user/manager");
		logger.info("view name:{}", mav.getViewName());
		return mav;
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.POST)
	public @ResponseBody DatatableDto<UserModel> all(BaseDatatableDto dto) throws SQLException {
		List<UserModel> rtv = userService.allUser();
		DatatableDto<UserModel> data = new DatatableDto<UserModel>(rtv, dto);
		return data;
	}
}
