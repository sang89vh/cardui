package com.card.api.CardUI.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/admin")
public class AdminController extends BaseController{

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

	@RequestMapping(value = "/index")
	public String index(Model model) {

		logger.info("go into admin=>index");
		return "redirect:/admin/card";
	}
	
	@RequestMapping(value = "/card")
	public ModelAndView  card(ModelAndView mav) {

		logger.info("go into admin=>card");
		mav.addObject("user",getPrincipal());
		
		mav.setViewName("admin/card");
		return mav;
	}

}
