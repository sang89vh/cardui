package com.card.api.CardUI.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes("authController")
@RequestMapping("/auth")
public class AuthController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(ModelAndView mav, @RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			logger.error("########################################################");
			logger.error("auth/login GET =>:{}",getPrincipal());
			logger.error("########################################################");
			mav.setViewName("redirect:/admin/index");
			return mav;
		}
		logger.error("auth/login GET AnonymousAuthenticationToken=>:{}",getPrincipal());
		
		logger.info("error:{}", error);
		if (error != null) {
			mav.addObject("error", error);
		}

		if (logout != null) {
			mav.setViewName("redirect:/auth/logout");
		} else {
			mav.setViewName("auth/login");
		}

		logger.info("view name:{}",mav.getViewName());
		return mav;
	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView doLogin(ModelAndView mav, @RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth instanceof AnonymousAuthenticationToken) {
			logger.error("########################################################");
			logger.error("auth/login POST => AnonymousAuthenticationToken:{}",getPrincipal());
			logger.error("########################################################");
		}
		logger.info("error:{}", error);
		if (error != null) {
			logger.error("error:{}",error);
			mav.addObject("error",error);
			mav.setViewName("auth/login");
			
			return mav;
		}
		
		if (logout != null) {
			mav.setViewName("auth/logout");
		} else {
			mav.setViewName("redirect:/admin/index");
		}
		
		return mav;
	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {
		logger.error("============================================================================");
		logger.info("403");
		logger.error("============================================================================");
		ModelAndView model = new ModelAndView();

		// check if auth is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			logger.info("userDetail:{}",userDetail);

			model.addObject("authname", userDetail.getUsername());
			model.addObject("role", userDetail.getAuthorities());
			model.setViewName("auth/403");
		}
//		else {
//			logger.error("auth/403 GET => AnonymousAuthenticationToken:{}",getPrincipal());
//			model.setViewName("redirect:/auth/login");
//		}
		
		model.addObject("auth", getPrincipal());
		return model;

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		logger.error("########################################################");
		logger.error("auth/logout");
		logger.error("########################################################");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/auth/login";// You can redirect wherever you want, but generally it's a good practice
												// to show login screen again.
	}
}
