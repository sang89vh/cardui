package com.card.api.CardUI.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author SangNV3
 * @since 20/10/2017
 */

public class BaseDatatableDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = -6461630204946530653L;
	private Integer start;
	private Integer draw;
	private Integer length;
	private List<DatatableColumnDto> columns;
	private List<DatatableOrderDto> order;
	private DatatableSearchDto search;
	private String username;

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getDraw() {
		return draw;
	}

	public void setDraw(Integer draw) {
		this.draw = draw;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public List<DatatableOrderDto> getOrder() {
		return order;
	}

	public void setOrder(List<DatatableOrderDto> order) {
		this.order = order;
	}

	public DatatableSearchDto getSearch() {
		return search;
	}

	public void setSearch(DatatableSearchDto search) {
		this.search = search;
	}

	public void setColumns(List<DatatableColumnDto> columns) {
		this.columns = columns;
	}

	public List<DatatableColumnDto> getColumns() {
		return columns;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	

}
