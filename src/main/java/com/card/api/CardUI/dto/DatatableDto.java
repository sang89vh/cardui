package com.card.api.CardUI.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author SangNV3
 * @since 20/10/2017
 */

public class DatatableDto<T> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3810027372180964603L;
	private List<T> data;
	private Integer recordsFiltered;
	private Integer draw;
	private Integer recordsTotal;
	public DatatableDto(List<T> data,BaseDatatableDto dto) {
		super();
		this.draw = dto.getDraw();
		this.data=data;
		if(this.data != null && this.data.size()>0) {
			this.recordsFiltered = data.size();
			this.recordsTotal = data.size();
		}else {
			this.data = new ArrayList<T>();
			this.recordsFiltered = 0;
			this.recordsTotal = 0;
		}
	}
	public DatatableDto() {
		super();
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public Integer getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(Integer recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public Integer getDraw() {
		return draw;
	}

	public void setDraw(Integer draw) {
		this.draw = draw;
	}

	public Integer getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(Integer recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

}
