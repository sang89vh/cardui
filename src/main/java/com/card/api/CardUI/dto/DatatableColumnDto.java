package com.card.api.CardUI.dto;

import java.io.Serializable;
/**
 * 
 * @author SangNV3
 * @since 20/10/2017
 */

public class DatatableColumnDto implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -981571562078933129L;
	private String data;
	private String  name;
	private Boolean  orderable;
	private DatatableSearchDto search;
	private Boolean  searchable;
	/**
	 * 
	 * @return name of column table on Datatabase !important things
	 */
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getOrderable() {
		return orderable;
	}
	public void setOrderable(Boolean orderable) {
		this.orderable = orderable;
	}
	public DatatableSearchDto getSearch() {
		return search;
	}
	public void setSearch(DatatableSearchDto search) {
		this.search = search;
	}
	public Boolean getSearchable() {
		return searchable;
	}
	public void setSearchable(Boolean searchable) {
		this.searchable = searchable;
	}
	
	

}
