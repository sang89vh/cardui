package com.card.api.CardUI.dto;

import java.io.Serializable;
/**
 * 
 * @author SangNV3
 * @since 20/10/2017
 */

public class DatatableSearchDto implements Serializable{	

	/**
	 * 
	 */
	private static final long serialVersionUID = 3312259254574330842L;
	private Boolean regex;
	private String value;
	public Boolean getRegex() {
		return regex;
	}
	public void setRegex(Boolean regex) {
		this.regex = regex;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
