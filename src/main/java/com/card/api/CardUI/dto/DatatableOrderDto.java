package com.card.api.CardUI.dto;

import java.io.Serializable;
/**
 * 
 * @author SangNV3
 * @since 20/10/2017
 */

public class DatatableOrderDto implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8047772558591886452L;
	private Integer column;
	private String dir;
	public Integer getColumn() {
		return column;
	}
	public void setColumn(Integer column) {
		this.column = column;
	}
	public String getDir() {
		return dir;
	}
	public void setDir(String dir) {
		this.dir = dir;
	}
}
