package com.card.api.CardUI.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.card.api.CardUI.dao.IUserDao;
import com.card.api.CardUI.model.UserModel;
import com.card.api.CardUI.service.IUserService;

@Service("UserServiceImpl")
public class UserServiceImpl implements IUserService {

	@Autowired
	private IUserDao userDao;
	
	public void isertUser() throws SQLException {
		// TODO Auto-generated method stub

	}

	public List<UserModel> getAllUser() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public List<UserModel> allUser() throws SQLException {
		List<UserModel> allUser = userDao.allUser();
		if(allUser == null) {
			allUser = new ArrayList<UserModel>();
		}
		return allUser;
	}

}
