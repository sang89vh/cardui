package com.card.api.CardUI.service;

import java.sql.SQLException;
import java.util.List;

import com.card.api.CardUI.model.UserModel;

public interface IUserService {
	public List<UserModel> allUser() throws SQLException;
	public void isertUser() throws SQLException;
	public List<UserModel> getAllUser() throws SQLException ;
}
