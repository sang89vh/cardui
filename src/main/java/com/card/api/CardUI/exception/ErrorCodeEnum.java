package com.card.api.CardUI.exception;
public interface ErrorCodeEnum {
	
	String  getErrorCode();
	String  getMessageCode();
}
