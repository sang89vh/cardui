package com.card.api.CardUI.exception;
import java.util.List;

public class CardUIException extends BaseException {
	
	private static final long serialVersionUID = 8823356956725033191L;

	public CardUIException(ErrorCodeEnum errorCodeEnum) {
		super();
		this.setErrorCode(errorCodeEnum);
	}
	
	public CardUIException(ErrorCodeEnum errorCodeEnum,List<FieldError> fieldErrors) {
		super();
		this.setErrorCode(errorCodeEnum);
		this.setFieldErrors(fieldErrors);
	}
	


}
