package com.card.api.CardUI.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
@Builder
public class UserModel extends BaseModel {
	private static final long serialVersionUID = 1L;
	private Integer user_id;
	private String user_name;
	private String password;
	private Integer enabled;
	private Integer user_role_id;
	private String role;

}
