package com.card.api.CardUI.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.card.api.CardUI.model.BaseModel;

public interface ICardUIDao {
	public <E> List<E> selectList(String name, BaseModel parameter)
			throws SQLException;

	public Map<?, ?> getMap(String name, String mapKey) throws SQLException;

	public int insert(String name, BaseModel prm) throws SQLException;

	public Object selectOne(String name, BaseModel parameter)
			throws SQLException;

	public int update(String name, BaseModel parameter) throws SQLException;
	public int delete(String name, BaseModel parameter) throws SQLException;
}
