package com.card.api.CardUI.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Component;

import com.card.api.CardUI.dao.IUserDao;
import com.card.api.CardUI.model.UserModel;
@Component("userDaoImpl")
public class UserDaoImpl extends BaseDao implements IUserDao{

	protected UserDaoImpl() {
		super("user");
	}


	public List<UserModel> allUser() throws SQLException {
		return selectList("allUser");
	}
	public List<UserModel> findUser(UserModel prm) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public void insertUser(UserModel prm) throws SQLException {
		// TODO Auto-generated method stub
		
	}

}
