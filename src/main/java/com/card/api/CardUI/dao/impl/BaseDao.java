package com.card.api.CardUI.dao.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.card.api.CardUI.dao.ICardUIDao;
import com.card.api.CardUI.model.BaseModel;

public class BaseDao extends SqlSessionDaoSupport implements ICardUIDao {
	
	private static final Logger logger = LoggerFactory.getLogger(BaseDao.class);
	
	protected String mNamespace;

	protected BaseDao(String namespace) {
		mNamespace = namespace;
	}

	@Override
	@Resource(name = "sqlSessionFactory")
	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		super.setSqlSessionFactory(sqlSessionFactory);
	}
	public int insert(String name, BaseModel prm) throws SQLException {
		return getSqlSession().insert(mNamespace + "." + name, prm);
	}
	public <E> List<E> selectList(String name, BaseModel prm)
			throws SQLException {
		List<E> data = getSqlSession().selectList(mNamespace + "." + name, prm);
		return data;
	}
	public <E> List<E> selectList(String name)
			throws SQLException {
		List<E> data = getSqlSession().selectList(mNamespace + "." + name);
		return data;
	}

	public Object selectOne(String name, BaseModel parameter)
			throws SQLException {
		return getSqlSession().selectOne(mNamespace + "." + name, parameter);
	}
	public Object selectOne(String name)
			throws SQLException {
		return getSqlSession().selectOne(mNamespace + "." + name);
	}

	public Map<?, ?> getMap(String name, String mapKey) throws SQLException {
		return getSqlSession().selectMap(mNamespace + "." + name, mapKey);
	}


	public int update(String name, BaseModel parameter) throws SQLException {
		return getSqlSession().update(mNamespace + "." + name, parameter);
	}
	
	public int delete(String name, BaseModel parameter) throws SQLException {
		return getSqlSession().delete(mNamespace+"."+name, parameter);
	}
	
	public int delete(String name) throws SQLException {
		return getSqlSession().delete(mNamespace+"."+name);
	}

}
