package com.card.api.CardUI.dao;

import java.sql.SQLException;
import java.util.List;

import com.card.api.CardUI.model.UserModel;

public interface IUserDao {
	public List<UserModel> allUser() throws SQLException;
	public List<UserModel> findUser(UserModel prm) throws SQLException;
	public void insertUser(UserModel prm) throws SQLException;
}
