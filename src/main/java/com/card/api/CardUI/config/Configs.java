package com.card.api.CardUI.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Configs implements CommonCode {
	
	public static String example;

	@Value("${example}")
	public void setCmpanyProfileUrl(String url) {
		example = url;
	}

}
